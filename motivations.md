* Normal QWERTY layout
* home row modifiers
* no single hand chording
* easy workspace switching
* easy tmux movements

# Shortcuts
* WIN + NUM (workspace switching)
* CTRL + a then NUM (tmux pane switching)
* CTRL + a then s (tmux session selection)
* CTRL + a then c (tmux create pane)
* CTRL + a then "/% (tmux split pane)
* CTRL + h/j/k/l (vim/tmux window movements)
* CTRL + ] (vim tags movements)
* TAB for rotating through vim snippets
